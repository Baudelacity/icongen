package com.baudelacity.icongenerator;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Bitmap mainImage, scaled_icon, mask, overlay, highlight, loaded_icon, result;
    Boolean boolean_custom_icon = false, maskIsChecked = false;
    ImageView icon, icon2, icon3;
    Drawable myIcon;
    protected static final int PICK_IMAGE = 0, PICK_BACKGROUND = 1;
    Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        icon = (ImageView) findViewById(R.id.icon_image);
        icon2 = (ImageView) findViewById(R.id.icon_image2);
        icon3 = (ImageView) findViewById(R.id.icon_image3);
        button = (Button) findViewById(R.id.load_button);
        int a = 170;
        int b = 100;
        Drawable drawable = getResources().getDrawableForDensity(R.mipmap.ic_launcher, DisplayMetrics.DENSITY_XXHIGH);
        //Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
        //icon.setImageDrawable(icon_generator_main(drawable));
        icon.setImageDrawable(scaledIcon(icon_generator_main(drawable), a));
        //icon2.setImageDrawable(icon_generator_main(drawable));
        //icon3.setImageDrawable(icon_generator_main(drawable));
        icon2.setImageDrawable(scaledIcon(icon_generator_main(drawable), a));
        icon3.setImageDrawable(scaledIcon(icon_generator_main(drawable), b));

    }

        Drawable icon_generator_main(Drawable icon) {
            import_to_generator(icon);
            create_scaled_icon(boolean_custom_icon, loaded_icon, mask);
            Canvas canvas = new Canvas();
            canvas.setBitmap(result);
            Paint paint = new Paint();
            canvas.save();
            icon_setXfermode(paint, boolean_custom_icon, canvas );
            canvas.restore();
            Drawable d = new BitmapDrawable(result);
            return d;
        }
        void import_to_generator(Drawable icon) {

            mainImage = BitmapFactory.decodeResource(getResources(),
                    R.mipmap.color);
            mask= BitmapFactory.decodeResource(getResources(),
                    R.mipmap.base);
            overlay = BitmapFactory.decodeResource(getResources(),
                    R.mipmap.overlay);
            highlight = BitmapFactory.decodeResource(getResources(),
                    R.mipmap.highlight);
            loaded_icon = ((BitmapDrawable)icon).getBitmap();
            result = Bitmap.createBitmap(mainImage.getWidth(), mainImage.getHeight(), Bitmap.Config.ARGB_8888);
        }
        Bitmap create_scaled_icon(boolean isChecked, Bitmap loaded_icon, Bitmap mask) {
                //scaled_icon = Bitmap.createScaledBitmap(loaded_icon, 55, 55, true);
            scaled_icon = Bitmap.createScaledBitmap(loaded_icon, 180, 180, true);
            return scaled_icon;
        }
        void icon_setXfermode(Paint paint, boolean IsChecked, Canvas canvas ) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
            paint.setFilterBitmap(true);
                canvas.drawBitmap(mainImage, 0, 0,paint);
                paint.setXfermode(null);
                canvas.drawBitmap(scaled_icon,  31 ,31,paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            canvas.drawBitmap(mask, 0, 0,paint);
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
                canvas.drawBitmap(highlight, 0, 0,paint);
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));
                canvas.drawBitmap(overlay, 0, 0,paint);
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
            paint.setXfermode(null);


        }

    Drawable scaledIcon(Drawable icona, int size) {
        Bitmap DrawIcon = ((BitmapDrawable)icona).getBitmap();
        Bitmap scale = Bitmap.createScaledBitmap(DrawIcon, ((int) convertDpToPixel(size, this)), ((int) convertDpToPixel(size, this)), true);
        Drawable dun = new BitmapDrawable(scale);
        return dun;
    }


    public float convertDpToPixel(float dp, Activity context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
    }


